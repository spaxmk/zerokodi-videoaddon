import sys
import os
import urllib
import xbmc
import xbmcgui
import xbmcplugin
import xbmcaddon
import logging
from operator import itemgetter

def show_tags():
  tag_handle = int(sys.argv[1])
  xbmcplugin.setContent(tag_handle, 'tags')

  for tag in tags:
    iconPath = os.path.join(home, 'logos', tag['icon'])
    li = xbmcgui.ListItem(tag['name'], iconImage=iconPath)
    url = sys.argv[0] + '?tag=' + str(tag['id'])
    xbmcplugin.addDirectoryItem(handle=tag_handle, url=url, listitem=li, isFolder=True)

  xbmcplugin.endOfDirectory(tag_handle)


def show_streams(tag):
  stream_handle = int(sys.argv[1])
  xbmcplugin.setContent(stream_handle, 'streams')
  logging.warning('TAG show_streams!!!! %s', tag)
  for stream in streams[str(tag)]:
    logging.debug('STREAM HERE!!! %s', stream['name'])
    iconPath = os.path.join(home, 'logos', stream['icon'])
    li = xbmcgui.ListItem(stream['name'], iconImage=iconPath)
    xbmcplugin.addDirectoryItem(handle=stream_handle, url=stream['url'], listitem=li)

  xbmcplugin.endOfDirectory(stream_handle)


def get_params():
  """
  Retrieves the current existing parameters from XBMC.
  """
  param = []
  paramstring = sys.argv[2]
  if len(paramstring) >= 2:
    params = sys.argv[2]
    cleanedparams = params.replace('?', '')
    if params[len(params) - 1] == '/':
      params = params[0:len(params) - 2]
    pairsofparams = cleanedparams.split('&')
    param = {}
    for i in range(len(pairsofparams)):
      splitparams = {}
      splitparams = pairsofparams[i].split('=')
      if (len(splitparams)) == 2:
        param[splitparams[0]] = splitparams[1]
  return param


def lower_getter(field):
  def _getter(obj):
    return obj[field].lower()

  return _getter


addon = xbmcaddon.Addon()
home = xbmc.translatePath(addon.getAddonInfo('path'))

tags = [
  {
    'name': 'Live TV',
    'id': 'LiveTV',
    'icon': 'livetv.png'
  }, {
    'name': 'Movies',
    'id': 'Movies',
    'icon': 'movies.png'
  }
]


LiveTV = [{
  'name': 'oriontelekom',
  'url': 'hhttp://iptv.oriontelekom.rs/SWF/flowplayer.commercial-3.2.18.swf?0.053180769054251176',
  'icon': 'Vevo Tv.png',
  'disabled': False
}, {
  'name': 'Naxi Radio 1',
  'url': 'naxi64ssl.streaming.rs:9162/;stream.nsv',
  'icon': 'NaxiRadio.png',
  'disabled': False
}, {
  'name': 'Prva',
  'url': 'blob:https://ustreamix.to/e08ebdab-ca50-49a4-9353-a10477af5d4e',
  'icon': 'NaxiRadio.png',
  'disabled': False
}, 
{
  'name': 'b92',
  'url': 'https://bit.ly/3g28l2w',
  'icon': 'NaxiRadio.png',
  'disabled': False
},
{
  'name': 'Prva',
  'url': 'http://www.tashtv.net/p/prva.html',
  'icon': 'NaxiRadio.png',
  'disabled': False
}, 
{
  'name': 'RTS1',
  'url': 'https://ustreamix.to/2d094670-a2c0-42a9-9ed3-9c1cf8503dea',
  'icon': 'NaxiRadio.png',
  'disabled': False
}, {
  'name': 'trakt',
  'url': 'https://api.trakt.tv/shows/trending',
  'icon': 'FX.png',
  'disabled': False
}, {
  'name': 'bbci1',
  'url': 'http://a.files.bbci.co.uk/media/live/manifesto/audio_video/simulcast/hds/uk/pc/llnw/bbc_news24.f4m',
  'icon': '.png',
  'disabled': False
}, {
  'name': 'gzcbn',
  'url': 'http://www.gzcbn.tv/app/?app=ios&controller=cmsapi&action=pindao',
  'icon': '.png',
  'disabled': False
}, {
  'name': 'addonname',
  'url': 'plugin://plugin.video.addonname/?action=get_list&content=0&url=http%3a%2f%2fnorestrictions.club%2fnorestrictions.club%2ftastreams%2ffiles%2fOld%2520School%2520Reggae.xml',
  'icon': '.png',
  'disabled': False
}, {
  'name': 'juststreaming1',
  'url': 'https://archive.org/download/juststreaming1/juststreaming1.zip',
  'icon': '.png',
  'disabled': False
}, {
  'name': 'Null',
  'url': 'Null',
  'icon': '.png',
  'disabled': False
}]


Movies = [{
  'name': 'Null',
  'url': '',
  'icon': 'Null',
  'disabled': False
}]


streams = {
  'LiveTV': sorted((i for i in LiveTV if not i.get('disabled', False)), key=lower_getter('name')),
  'Movies': sorted((i for i in Movies if not i.get('disabled', False)), key=lower_getter('name')),
  # 'LiveTV': sorted(LiveTV, key=lower_getter('name')),
  # 'Movies': sorted(Movies, key=lower_getter('name')),
}

PARAMS = get_params()
TAG = None
logging.warning('PARAMS!!!! %s', PARAMS)

try:
  TAG = PARAMS['tag']
except:
  pass

logging.warning('ARGS!!!! sys.argv %s', sys.argv)

if TAG == None:
  show_tags()
else:
  show_streams(TAG)
